package com.zajac.adam.controller;

/**
 * Created by zajac on 09.10.2017.
 * Main controller
 */

import com.zajac.adam.feignClient.DataSource;
import com.zajac.adam.feignClient.Dispatcher;
import com.zajac.adam.feignClient.ImageProcessorBinarization;
import com.zajac.adam.feignClient.ImageProcessorColorsBinarization;
import com.zajac.adam.test.MainTester;
import com.zajac.adam.test.TestResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class BackendTestsRestController {

    private final Dispatcher dispatcher;
    private final DataSource dataSource;
    private final ImageProcessorBinarization imageProcessorBinarization;
    private final ImageProcessorColorsBinarization imageProcessorColorsBinarization;

    private boolean isTestsStarted = false;
    private boolean isFirstGet = true;
    private MainTester mainTester;
    private List<TestResult> testResults;

    @Autowired
    public BackendTestsRestController(Dispatcher dispatcher, DataSource dataSource,
                                      ImageProcessorBinarization imageProcessorBinarization,
                                      ImageProcessorColorsBinarization imageProcessorColorsBinarization) {
        this.dispatcher = dispatcher;
        this.dataSource = dataSource;
        this.imageProcessorBinarization = imageProcessorBinarization;
        this.imageProcessorColorsBinarization = imageProcessorColorsBinarization;
    }

    @GetMapping("/getTestResults")
    @ResponseBody()
    public List<TestResult> getTestResults() {
        if (this.isFirstGet) {
            prepareToTest();
            this.isFirstGet = false;
        }
        return testResults;
    }

    private void prepareToTest() {

        mainTester = new MainTester(dispatcher, dataSource, imageProcessorBinarization,
                imageProcessorColorsBinarization);
        testResults = mainTester.getPreparedTestResults();

    }

    @PostMapping("/start")
    public void startTest() {
        try {
            if (!isTestsStarted) {
                isTestsStarted = true;
                if (!this.isFirstGet)
                    prepareToTest();
                mainTester.startTests();
            }
        } finally {
            isTestsStarted = false;
        }
    }
}
