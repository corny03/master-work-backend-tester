package com.zajac.adam.feignClient;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by zajac on 10.08.2017.
 * Data Source Feign Client
 */

@FeignClient(name = "DATA-SOURCE")
public interface DataSource extends MyName {
    @GetMapping("/{id}")
    ResponseEntity<ByteArrayResource> getImage(@PathVariable("id") String id);

    @PostMapping(value = "/upload")
    ResponseEntity<String> singleUpload(@RequestParam("filename") String fileName,
                                        @RequestBody() byte[] bytes,
                                        @RequestParam("userName") String userName,
                                        @RequestParam("isHidden") boolean isHidden);

    @DeleteMapping(value = "/{id}", produces = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity deleteById(@PathVariable("id") String id);
}
