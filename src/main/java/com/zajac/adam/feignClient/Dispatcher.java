package com.zajac.adam.feignClient;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by zajac on 11.10.2017.
 */
@FeignClient(name = "dispatcher")
public interface Dispatcher extends MyName {

    @PostMapping("/upload")
    ResponseEntity<String> singleUpload(@RequestParam("filename") String fileName,
                                        @RequestBody byte[] bytes,
                                        @RequestParam("userName") String userName,
                                        @RequestParam("isHidden") boolean isHidden);

    @PostMapping("/uploadMultipartFile")
    ResponseEntity<String> singleUpload(@RequestPart("file") MultipartFile file,
                                        @RequestParam("userName") String userName,
                                        @RequestParam("isHidden") Boolean isHidden);

    @DeleteMapping("/delete-image/{id}")
    ResponseEntity deleteById(@PathVariable("id") String id);
}
