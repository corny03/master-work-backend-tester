package com.zajac.adam.feignClient;

/**
 * Created by zajac on 20.08.2017.
 */
public interface MyName {
    default String getMyName(){
        return this.toString().split("name=")[1].split(",")[0];
    }
}
