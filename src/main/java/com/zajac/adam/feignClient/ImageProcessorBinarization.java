package com.zajac.adam.feignClient;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by zajac on 19.08.2017.
 */
@FeignClient(name = "image-processor-binarization")
public interface ImageProcessorBinarization extends FilterServiceInterface {

    @GetMapping("{id}")
    ResponseEntity<ByteArrayResource> getImage(@PathVariable("id") String id);

    @PostMapping("/upload")
    ResponseEntity singleUpload(@RequestParam("id") String id, @RequestParam("filename") String fileName, @RequestBody byte[] bytes);

    @DeleteMapping("{id}")
    ResponseEntity deleteById(@PathVariable("id") String id);
}