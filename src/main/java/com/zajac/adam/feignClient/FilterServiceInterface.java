package com.zajac.adam.feignClient;

import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.ResponseEntity;

/**
 * Created by zajac on 05.11.2017.
 */

public interface FilterServiceInterface {
    default String getMyName() {
        return this.toString().split("name=")[1].split(",")[0];
    }

    ResponseEntity<ByteArrayResource> getImage(String id);

    ResponseEntity singleUpload(String id, String fileName, byte[] bytes);

    ResponseEntity deleteById(String id);
}
