package com.zajac.adam.test;

import com.google.common.io.ByteStreams;
import com.zajac.adam.feignClient.*;
import feign.FeignException;
import feign.RetryableException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.ConnectException;

/**
 * Created by zajac on 11.10.2017.
 * Tester prepared to test all system
 */
@Service
class FullSystemTester {

    private final Dispatcher dispatcherService;
    private final DataSource dataSource;
    private final ImageProcessorBinarization imageProcessorBinarization;
    private final ImageProcessorColorsBinarization imageProcessorColorsBinarization;
    private final String userName = "BackendTester";
    private final boolean isHidden = true;
    private final String fileName = "lena.jpg";
    private String idImage = null;
    @Value(value = "classpath:lena.jpg")
    private Resource imageResource;

    FullSystemTester(Dispatcher dispatcherService, DataSource dataSource,
                     ImageProcessorBinarization imageProcessorBinarization,
                     ImageProcessorColorsBinarization imageProcessorColorsBinarization) {
        this.dispatcherService = dispatcherService;
        this.dataSource = dataSource;
        this.imageProcessorBinarization = imageProcessorBinarization;
        this.imageProcessorColorsBinarization = imageProcessorColorsBinarization;
    }

    void startTest(TestResult sendTestResult,
                   TestResult sentSuccessTestResult,
                   TestResult sentToBinarizeFilterTestResult,
                   TestResult sentToColorBinarizeFilterTestResult,
                   TestResult deletingTestResult) {
        sendTestResult.setStatus(Status.PROCESSING);
        try {
            byte[] bytes = ByteStreams.toByteArray(this.getClass().getClassLoader().getResourceAsStream(fileName));
            ResponseEntity<String> longResponseEntity = dispatcherService.singleUpload(fileName, bytes, userName,
                    isHidden);
            if (longResponseEntity.getStatusCode().is2xxSuccessful()) {
                sendTestResult.setStatus(Status.DONE);
                sendTestResult.setDescription("wysłano z powodzeniem");
                this.idImage = longResponseEntity.getBody();
                this.dispatcherSentSuccessTest(sentSuccessTestResult);
                this.dataInFilterTest(this.imageProcessorBinarization, sentToBinarizeFilterTestResult);
                this.dataInFilterTest(this.imageProcessorColorsBinarization, sentToColorBinarizeFilterTestResult);
                this.dispatcherDeletingTest(deletingTestResult);
            } else {
                sendTestResult.setDescription("błąd " + longResponseEntity.getStatusCode().getReasonPhrase());
                sendTestResult.setStatus(Status.FAILED);
            }
        } catch (RetryableException | ConnectException e) {
            sendTestResult.setStatus(Status.FAILED);
            sendTestResult.setDescription("problem z połączeniem do usługi " + dispatcherService.getMyName());
        } catch (IOException e) {
            sendTestResult.setStatus(Status.FAILED);
            sendTestResult.setDescription("problem z odczytem pliku w testerze " + e.getMessage());
        } catch (FeignException e) {
            sendTestResult.setStatus(Status.FAILED);
            sendTestResult.setDescription("problem z " + dispatcherService.getMyName() + " " + e.getMessage());
        } catch (RuntimeException e) {
            sendTestResult.setStatus(Status.FAILED);
            sendTestResult.setDescription("problem z połączeniem do usługi " + dispatcherService.getMyName() +
                    " (prawdopodobnie usługa nie zarejestrowana)");
        }
    }

    private void dispatcherSentSuccessTest(TestResult sentSuccessResult) {
        sentSuccessResult.setStatus(Status.PROCESSING);
        try {
            ResponseEntity<ByteArrayResource> responseEntity = dataSource.getImage(this.idImage);
            byte[] imageBytes = responseEntity.getBody().getByteArray();

            if (responseEntity.getStatusCode().is2xxSuccessful()) {
                if (imageBytes.length <= 1)
                    throw new ArrayIndexOutOfBoundsException();
                sentSuccessResult.setStatus(Status.DONE);
                sentSuccessResult.setDescription("Pomyślnie pobrano");
            } else {
                sentSuccessResult.setDescription("błąd " + responseEntity.getStatusCode().getReasonPhrase());
                sentSuccessResult.setStatus(Status.FAILED);
            }
        } catch (RetryableException e) {
            sentSuccessResult.setStatus(Status.FAILED);
            sentSuccessResult.setDescription("problem z połączeniem do usługi " + dataSource.getMyName());
        } catch (NullPointerException | ArrayIndexOutOfBoundsException e) {
            sentSuccessResult.setStatus(Status.FAILED);
            sentSuccessResult.setDescription("problem z danymi z magazynu danych " +
                    e.getMessage());
        } catch (FeignException e) {
            sentSuccessResult.setStatus(Status.FAILED);
            sentSuccessResult.setDescription("problem z " + dataSource.getMyName() + " " + e.getMessage());
        } catch (RuntimeException e) {
            sentSuccessResult.setStatus(Status.FAILED);
            sentSuccessResult.setDescription("problem z połączeniem do usługi " + dataSource.getMyName() +
                    " (prawdopodobnie usługa nie zarejestrowana)");
        }
    }

    private void dataInFilterTest(FilterServiceInterface someFilter, TestResult sentToFiltersResult) {
        sentToFiltersResult.setStatus(Status.PROCESSING);
        String filterName = someFilter.getMyName();
        try {
            ResponseEntity<ByteArrayResource> filterImageResponseEntity = someFilter.getImage(this.idImage);
            if (filterImageResponseEntity.getStatusCode().is2xxSuccessful()) {
                byte[] filterImageBytes = filterImageResponseEntity.getBody().getByteArray();
                if (filterImageBytes.length <= 1)
                    throw new ArrayIndexOutOfBoundsException();
                sentToFiltersResult.setStatus(Status.DONE);
                sentToFiltersResult.setDescription("Pomyślnie pobrano " + filterName);
            } else {
                sentToFiltersResult.setDescription("błąd " + filterImageResponseEntity.getStatusCode()
                        .getReasonPhrase());
                sentToFiltersResult.setStatus(Status.FAILED);
            }
        } catch (RetryableException e) {
            sentToFiltersResult.setStatus(Status.FAILED);
            sentToFiltersResult.setDescription("problem z połączeniem do usługi " + filterName);
        } catch (NullPointerException | ArrayIndexOutOfBoundsException e) {
            sentToFiltersResult.setStatus(Status.FAILED);
            sentToFiltersResult.setDescription("problem z danymi z magazynu danych " + filterName);
        } catch (FeignException e) {
            if (e.status() == HttpStatus.FOUND.value()) {
                sentToFiltersResult.setStatus(Status.DONE);
                sentToFiltersResult.setDescription("znaleziono zdjęcie, lecz jeszcze nie przetworzono. Usługa " + filterName);
            } else {
                sentToFiltersResult.setStatus(Status.FAILED);
                sentToFiltersResult.setDescription("problem z filtrem " + filterName);
            }
        } catch (RuntimeException e) {
            sentToFiltersResult.setStatus(Status.FAILED);
            sentToFiltersResult.setDescription("problem z połączeniem do usługi " + filterName +
                    " (prawdopodobnie usługa nie zarejestrowana)");
        }
    }

    private void dispatcherDeletingTest(TestResult deletingTestResult) {
        deletingTestResult.setStatus(Status.PROCESSING);
        try {
            ResponseEntity deleteResponse = dispatcherService.deleteById(this.idImage);

            if (deleteResponse.getStatusCode().is2xxSuccessful()) {
                deleteFromFilterCheck(this.imageProcessorBinarization, deletingTestResult);
                if (deletingTestResult.getStatus() == Status.FAILED)
                    return;
                deleteFromFilterCheck(this.imageProcessorColorsBinarization, deletingTestResult);
                if (deletingTestResult.getStatus() == Status.FAILED)
                    return;
                deletingTestResult.setDescription("posprzątano po testach");
            } else {
                deletingTestResult.setDescription("błąd " + deleteResponse.getStatusCode());
                deletingTestResult.setStatus(Status.FAILED);
            }
        } catch (RetryableException e) {
            deletingTestResult.setStatus(Status.FAILED);
            deletingTestResult.setDescription("problem z połączeniem do usługi " + dispatcherService.getMyName());
        } catch (NullPointerException | ArrayIndexOutOfBoundsException e) {
            deletingTestResult.setStatus(Status.FAILED);
            deletingTestResult.setDescription("problem z danymi z magazynu danych "
                    + e.getMessage());
        } catch (FeignException e) {
            deletingTestResult.setStatus(Status.FAILED);
            deletingTestResult.setDescription("problem z " + dispatcherService.getMyName() + " "
                    + e.getMessage());
        } catch (RuntimeException e) {
            deletingTestResult.setStatus(Status.FAILED);
            deletingTestResult.setDescription("problem z połączeniem do usługi "
                    + dispatcherService.getMyName() + " (prawdopodobnie usługa nie zarejestrowana)");
        }
    }

    private void deleteFromFilterCheck(FilterServiceInterface filter, TestResult deletingTestResult) {

        TestResult failedTestResult = new TestResult("Niepowodzenie testu");
        dataInFilterTest(filter, failedTestResult);
        if (isFilterCleaned(failedTestResult))
            deletingTestResult.setStatusAndDescription(Status.DONE, filter.getMyName()
                    + " filtr posprzątał po tescie");
        else
            deletingTestResult.setStatusAndDescription(Status.FAILED, filter.getMyName()
                    + " filtr nie posprzątał wszystkiego po tescie");
    }

    private boolean isFilterCleaned(TestResult testResult) {
        return testResult.getStatus() == Status.FAILED;
    }
}
