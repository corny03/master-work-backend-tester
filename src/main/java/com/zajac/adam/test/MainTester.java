package com.zajac.adam.test;

import com.zajac.adam.feignClient.DataSource;
import com.zajac.adam.feignClient.Dispatcher;
import com.zajac.adam.feignClient.ImageProcessorBinarization;
import com.zajac.adam.feignClient.ImageProcessorColorsBinarization;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zajac on 11.10.2017.
 * Main Tester prepare and turn on tests
 */
@Service
public class MainTester {
    private final Dispatcher dispatcher;
    private final DataSource dataSource;
    private final ImageProcessorBinarization imageProcessorBinarization;
    private final ImageProcessorColorsBinarization imageProcessorColorsBinarization;
    private TestResult sendTestResult;
    private TestResult sentSuccessTestResult;
    private TestResult sentToBinarizeFilterTestResult;
    private TestResult sentToColorBinarizeFilterTestResult;
    private TestResult deletingTestResult;
    private TestResult sendToDataSourceTestResult;
    private TestResult autoDownloadFunctionInBinarizedFilterTestResult;
    private TestResult deletingAfterFilterTestTestResult;
    private TestResult autoDownloadFunctionInColorBinarizedFilterTestResult;

    public MainTester(Dispatcher dispatcher, DataSource dataSource,
                      ImageProcessorBinarization imageProcessorBinarization,
                      ImageProcessorColorsBinarization imageProcessorColorsBinarization) {
        this.dispatcher = dispatcher;
        this.dataSource = dataSource;
        this.imageProcessorBinarization = imageProcessorBinarization;
        this.imageProcessorColorsBinarization = imageProcessorColorsBinarization;
    }

    public List<TestResult> getPreparedTestResults() {
        List<TestResult> preparedTestResults = new ArrayList<>();
        filtersAndDataSourcePreparation(preparedTestResults);
        fullSystemTestPreparation(preparedTestResults);
        return preparedTestResults;
    }

    private void filtersAndDataSourcePreparation(List<TestResult> preparedTestResults) {
        this.sendToDataSourceTestResult =
                new TestResult("sprawdzanie przysyłania zdjęć do magazynu danych (data-source)");
        preparedTestResults.add(sendToDataSourceTestResult);
        this.autoDownloadFunctionInBinarizedFilterTestResult = new TestResult("sprawdzanie autouzupełniania " +
                "brakującego zdjęcia z magazynu danych (data-source) w filtrze (binarization-filter)");
        preparedTestResults.add(autoDownloadFunctionInBinarizedFilterTestResult);
        this.autoDownloadFunctionInColorBinarizedFilterTestResult =
                new TestResult("sprawdzanie autouzupełniania " +
                        "brakującego zdjęcia z magazynu danych (data-source) w filtrze (color-binarization-filter)");
        preparedTestResults.add(autoDownloadFunctionInColorBinarizedFilterTestResult);
        this.deletingAfterFilterTestTestResult = new TestResult("Czyszczenie danych po teście");
        preparedTestResults.add(deletingAfterFilterTestTestResult);
    }

    private void fullSystemTestPreparation(List<TestResult> preparedTestResults) {
        this.sendTestResult = new TestResult("wysyłanie przez zarządce (dispatcher), do magazynu danych (data-source)");
        preparedTestResults.add(sendTestResult);
        this.sentSuccessTestResult = new TestResult("sprawdzenie zapisu w magazynie (data-source)");
        preparedTestResults.add(sentSuccessTestResult);
        this.sentToBinarizeFilterTestResult = new TestResult("sprawdzenie czy filtr ma zdjęcie (binarization-filter)");
        preparedTestResults.add(sentToBinarizeFilterTestResult);
        this.sentToColorBinarizeFilterTestResult = new TestResult("sprawdzenie czy filtr ma zdjęcie (color-binarization-filter)");
        preparedTestResults.add(sentToColorBinarizeFilterTestResult);
        this.deletingTestResult = new TestResult("czyszczenie danych po testach");
        preparedTestResults.add(deletingTestResult);
    }

    public void startTests() {
        FiltersAutoCompleteTester filtersAutoCompleteTester = new FiltersAutoCompleteTester(dataSource,
                imageProcessorBinarization,
                imageProcessorColorsBinarization);
        FullSystemTester fullSystemTester = new FullSystemTester(dispatcher,
                dataSource,
                imageProcessorBinarization,
                imageProcessorColorsBinarization);

        filtersAutoCompleteTester.startTest(this.sendToDataSourceTestResult,
                this.autoDownloadFunctionInBinarizedFilterTestResult,
                this.autoDownloadFunctionInColorBinarizedFilterTestResult,
                this.deletingAfterFilterTestTestResult);
        fullSystemTester.startTest(this.sendTestResult,
                this.sentSuccessTestResult,
                this.sentToBinarizeFilterTestResult,
                this.sentToColorBinarizeFilterTestResult,
                this.deletingTestResult);
    }
}

