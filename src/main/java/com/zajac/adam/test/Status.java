package com.zajac.adam.test;

/**
 * Created by zajac on 13.10.2017.
 */
public enum Status {
    DONE, PROCESSING, FAILED, NOTSTARTED
}