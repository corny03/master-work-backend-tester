package com.zajac.adam.test;

import com.google.common.io.ByteStreams;
import com.zajac.adam.feignClient.DataSource;
import com.zajac.adam.feignClient.FilterServiceInterface;
import com.zajac.adam.feignClient.ImageProcessorBinarization;
import com.zajac.adam.feignClient.ImageProcessorColorsBinarization;
import feign.FeignException;
import feign.RetryableException;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.net.ConnectException;

/**
 * Created by zajac on 11.10.2017.
 * Tester created to test second functionality of filters - auto complete missing filtered images
 */

class FiltersAutoCompleteTester {
    private final DataSource dataSource;
    private final ImageProcessorBinarization imageProcessorBinarization;
    private final ImageProcessorColorsBinarization imageProcessorColorsBinarization;
    private final String fileName = "lena.jpg";
    private final String userName = "BackendTester";
    private final boolean isHidden = true;
    private final int timeForProcessing = 2500;
    private String idImage = null;

    FiltersAutoCompleteTester(DataSource dataSource,
                              ImageProcessorBinarization imageProcessorBinarization,
                              ImageProcessorColorsBinarization imageProcessorColorsBinarization) {
        this.dataSource = dataSource;
        this.imageProcessorBinarization = imageProcessorBinarization;
        this.imageProcessorColorsBinarization = imageProcessorColorsBinarization;
    }

    void startTest(TestResult dataSourceTestResult,
                   TestResult binarizationFilterAutodownloadTestResult,
                   TestResult colorBinarizationFilterAutodownloadTestResult,
                   TestResult cleaningTestResult) {
        dataSourceTestResult.setStatus(Status.PROCESSING);
        try {
            byte[] bytes = ByteStreams.toByteArray(this.getClass().getClassLoader().getResourceAsStream(fileName));
            ResponseEntity<String> responseEntity = dataSource.singleUpload(this.fileName, bytes, userName, isHidden);
            if (!responseEntity.getStatusCode().is2xxSuccessful()) {
                dataSourceTestResult.setStatusAndDescription(Status.FAILED,
                        "problem z wysyłaniem do magazynu danych");
                return;
            }
            idImage = responseEntity.getBody();
            ResponseEntity<ByteArrayResource> imageResponseEntity = dataSource.getImage(idImage);
            if (!hasBodyBytes(imageResponseEntity)) {
                dataSourceTestResult.setStatusAndDescription(Status.FAILED,
                        "problem z odczytem po wysłaniu do magazynu danych");
                return;
            }
            dataSourceTestResult.setStatusAndDescription(Status.DONE,
                    "magazyn danych (data-source) działa dobrze");
            filterAutoCompleteTest(imageProcessorBinarization, binarizationFilterAutodownloadTestResult);
            filterAutoCompleteTest(imageProcessorColorsBinarization, colorBinarizationFilterAutodownloadTestResult);
            cleanAfterTest(cleaningTestResult);

        } catch (RetryableException | ConnectException e) {
            dataSourceTestResult.setStatusAndDescription(Status.FAILED,
                    "Problem z połączeniem do usługi " + dataSource.getMyName());
        } catch (IOException e) {
            dataSourceTestResult.setStatusAndDescription(Status.FAILED,
                    "Problem z odczytem pliku w testerze " + e.getMessage());
        } catch (FeignException e) {
            dataSourceTestResult.setStatusAndDescription(Status.FAILED,
                    "Problem z " + dataSource.getMyName() + " " + e.getMessage());

        } catch (RuntimeException e) {
            dataSourceTestResult.setStatusAndDescription(Status.FAILED, "problem z połączeniem do usługi " +
                    dataSource.getMyName() +
                    " (prawdopodbnie usługa nie zarejstrowana)");
        }
    }

    private void filterAutoCompleteTest(FilterServiceInterface filterService, TestResult testResult) {
        String filterServiceName = filterService.getMyName();
        testResult.setStatusAndDescription(Status.PROCESSING, "próba pobrania przetworzonego obrazu");
        try {
            ResponseEntity<ByteArrayResource> imageResponseEntity = null;
            try {
                imageResponseEntity = filterService.getImage(this.idImage);
            } catch (FeignException e) {
                if (e.status() == HttpStatus.FOUND.value()) {
                    testResult.setDescription("znaleziono zdjęcie, oczekiwanie na przetworzenie " + this.timeForProcessing + "ms...");
                    Thread.sleep(this.timeForProcessing);
                } else {
                    testResult.setStatusAndDescription(Status.FAILED, "problem z " + filterServiceName + " " + e.getMessage());
                    return;
                }
            }
            imageResponseEntity = filterService.getImage(this.idImage);
            if (imageResponseEntity.getStatusCode().is2xxSuccessful()) {
                if (!hasBodyBytes(imageResponseEntity)) {
                    testResult.setStatusAndDescription(Status.FAILED,
                            "problem z odczytem danych z " + filterServiceName);
                    return;
                }
                testResult.setStatusAndDescription(Status.DONE, "Serwice " + filterServiceName +
                        " działa poprawnie");
            } else if (imageResponseEntity.getStatusCode() == HttpStatus.FOUND) {
                testResult.setStatusAndDescription(Status.FAILED, "zdjęcie wciąż procesowane," +
                        " bądź sewis nie działa poprawnie" +
                        "(aktualny czas dla procesowania: " + this.timeForProcessing + "ms)");
            } else {
                testResult.setStatusAndDescription(Status.FAILED, "coś nie tak z serwisem " +
                        filterServiceName + ", status : " + imageResponseEntity.getStatusCode());
            }
        } catch (RetryableException e) {
            testResult.setStatusAndDescription(Status.FAILED,
                    "problem z połączeniem do usługi " + filterServiceName);
        } catch (FeignException e) {
            if (e.status() == HttpStatus.FOUND.value()) {
                testResult.setStatusAndDescription(Status.FAILED, "zdjęcie wciąż procesowane," +
                        " bądź sewis nie działa poprawnie" +
                        "(aktualny czas dla procesowania: " + this.timeForProcessing + "ms)");
            } else
                testResult.setStatusAndDescription(Status.FAILED,
                        "problem z " + filterServiceName + " " + e.getMessage());

        } catch (RuntimeException e) {
            testResult.setStatusAndDescription(Status.FAILED, "problem z połączeniem do usługi " +
                    filterServiceName +
                    " (prawdopodbnie usługa nie zarejstrowana)");
        } catch (InterruptedException e) {
            testResult.setStatusAndDescription(Status.FAILED, "problem ze wątkiem testera");
            e.printStackTrace();
        }
    }

    private void cleanAfterTest(TestResult testResult) {
        testResult.setStatus(Status.PROCESSING);
        try {
            ResponseEntity dataSourceResponseEntity = dataSource.deleteById(this.idImage);
            ResponseEntity binarizationResponseEntity = imageProcessorBinarization.deleteById(this.idImage);
            ResponseEntity colorBinarizationResponseEntity = imageProcessorColorsBinarization.deleteById(this.idImage);
            if (dataSourceResponseEntity.getStatusCode().is2xxSuccessful() ||
                    binarizationResponseEntity.getStatusCode().is2xxSuccessful() ||
                    colorBinarizationResponseEntity.getStatusCode().is2xxSuccessful()) {
                testResult.setStatusAndDescription(Status.DONE, "czyszczenie po testach");
            } else
                testResult.setStatusAndDescription(Status.DONE, "coś poszło nie tak podczas czyszczenia danych");
        } catch (FeignException e) {
            testResult.setStatusAndDescription(Status.FAILED, "problem z jakimś serwisem");
        }
    }

    private boolean hasBodyBytes(ResponseEntity<ByteArrayResource> responseEntity) {
        return responseEntity.getBody().getByteArray().length > 1;
    }
}
