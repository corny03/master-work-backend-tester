package com.zajac.adam.test;

/**
 * Created by zajac on 13.10.2017.
 */
public class TestResult {
    private String testName;
    private String description;
    private Status status;

    public TestResult(String testName) {
        this.testName = testName;
        status = Status.NOTSTARTED;
        description = "Oczekiwanie";
    }

    public void setStatusAndDescription(Status status, String description) {
        this.status = status;
        this.description = description;
    }

    public String getTestName() {
        return testName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return this.testName + "\n         " +
                this.description + "\n         " +
                this.status.toString();
    }
}
